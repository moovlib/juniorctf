```
$ john --wordlist=wordlist file
Loaded 1 password hash (crypt, generic crypt(3) [?/64])
Press 'q' or Ctrl-C to abort, almost any other key for status
banane           (palpaguin)
1g 0:00:00:00 100% 3.448g/s 17.24p/s 17.24c/s 17.24C/s pomme..ananas
Use the "--show" option to display all of the cracked passwords reliably
Session completed
```
