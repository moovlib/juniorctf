Title: Secret Base 1
Category: Reverse
Score: 50
Description:


Hi! How are you? How about we slowly start *really cool tricks like reversing*?

Try and crack this program `getin`. 

- Download it from the CTF server (see below). 
- Go into the directory where you downloaded the file (remember? use `cd` command)
- Set it as executable:

```
$ chmod u+x ./getin
```

- Finally, run it on Linux:

```
$ ./getin  
=== This is a very secret base ===
Password:
```

Now you can try and guess the password... Good luck!

## Out of ideas?

Sometimes, the password is just written (hidden) in the program itself. How about displaying all messages included in the executable? You can do this in Unix with a command named `strings`

Try: `strings ./getin`

You will get several strings, many not very interesting at this point. But some other strings are more interesting. Look at those:


```
...
Penguin
=== This is a secret base ===
Password: 
 ACCESS GRANTED !!!
Huh. Go back to nursery school!
...
```

So if the password isn't `Huh`, nor `nursery`, nor `secret`, what could it be... :-) Try!

## Flag

Use the password to flag the challenge and collect your points.
