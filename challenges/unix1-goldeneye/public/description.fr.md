Title: Lancement Golden Eye
Category: Unix
Score: 50
Description:

Pour Boris Grishenko, la vie se résume à un ordinateur, et toutes les informations qu'il doit retenir se trouvent sur son ordinateur. Coup de bol, 007 vient de récupérer l'ordinateur portable de Boris, et de le passer aux services de Q.

Toi, tu travailles pour Q, et il va falloir retrouver les informations importantes qui sont sur le compte de Boris. Ces informations sont cachées dans des fichiers sur l'ordinateur de Boris. Tu as accès à l'ordinateur de Boris, mais il va falloir que tu trouves et lises les fichiers qui contiennent les informations secrètes.

## Accéder à l'ordinateur de Boris

Utilise SSH:

- port `2322`
- utilisateur: `boris`
- mot de passe: `computer`
- ordinateur: `192.168.0.8`

## Fichiers et répertoires

### Oui oui mais moi je veux trouver la solution !

Pour trouver la solution, tu as besoin de te promener dans les répertoires, et de lire des fichiers.
Dès que tu as compris comment:

- aller d'un répertoire à un autre
- lire un fichier à un emplacement donné

... tu sauras résoudre cette énigme.

Les paragraphes après t'aident à y arriver. 

### Arborescence

Sous Unix, tous les fichiers sont organisés dans une arborescence de répertoires. Arborescence, car c'est un peu comme un arbre. Il y a le tronc de l'arbre. Sur ordinateur ça s'appelle la *racine* (anglais: root) et cela correspond au répertoire `/`.

Puis partant de la racine, il y a des grosses branches. Ce sont les premiers répertoires. Par exemple, mettons `/etc` ou `/home`. Cela veut dire "dans le répertoire etc à partir de la racine".

Puis il y a de plus petites branches. Ce sont des répertoires au sein de ces premiers répertoires. Par exemple, `/home/tig`. Cela veut dire "dans le répertoire tig qui est dans le répertoire home à partir de la racine".

### Chemin (path en anglais)

Il y a deux façons de décrire un chemin, c'est à dire le trajet pour aller vers un répertoire ou un fichier donné. 

- **chemin absolu**. Tu décris comment aller dans le répertoire ou le fichier **depuis la racine**. Par exemple, `/home/tig/blah.txt`. **Un chemin absolu commence toujours par /**.

- **chemin relatif**. Tu décris comment aller dans le répertoire ou le fichier *depuis l'endroit où tu te trouves*. Mettons que tu sois dans `/home/tig`. Si tu veux aller dans `/etc`, il faut d'abord revenir dans le répertoire qui contient tig : c'est le répertoire home. Puis depuis le répertoire home, il faut aller à la racine, puis aller dans `/etc`. 
Un chemin relatif **ne commence jamais par /**.

Dans Unix, le répertoire dans lequel tu es actuellement s'appelle `.`.
Le répertoire "au dessus" de toi (ton "père") s'appelle `..`.

Imagine que dans le répertoire boris tu as:

```
./boris/
|-------- Dockerfile
|-------- john.conf
|-------- public
|         |---- description.md
|-------- words
```

Depuis le répertoire boris, le chemin relatif qui mène à `description.md` est : `./public/description.md`.
Depuis le répertoire public, le chemin relatif qui mène à `john.conf` de boris est : `../john.conf`


Depuis le répertoire boris, qui est `./././././Dockerfile` ? Réfléchis. `.` désigne le répertoire courant. Donc on ne change jamais de répertoire. C'est donc au final le fichier `Dockerfile`. D'ailleurs, plutot que d'écrire `./Dockerfile` on écrit parfois juste `Dockerfile` (c'est à dire le fichier Dockerfile dans le répertoire courant).

Quelle est la différence entre `./john.conf`, `john.conf` et `/john.conf`. Fais bien attention aux détails...
- `./john.conf` veut dire le fichier john.conf dans le répertoire courant.
- `john.conf` veut dire pareil. Donc c'est le meme.
- `/john.conf` commence par `/`. C'est un chemin absolu. C'est le fichier john.conf situé à la racine. C'est donc (probablement) un fichier différent, sauf si tu te trouves actuellement à la racine...






### Changer de répertoire

- Pour se déplacer dans un répertoire, c'est la commande `cd` suivie du nom du répertoire. Tu peux spécifier un *chemin absolu* ou *relatif*. 

Exemple de chemin absolu: `cd /etc`
Exemple de chemin relatif: `cd ./etc`.

Attention au final ces deux commandes peuvent te mener dans un répertoire différent. Car le deuxième va dans le répertoire etc à partir de l'endroit où tu es. Si tu es dans `/home/tig`, tu vas alors dans `/home/tig/etc`

### Voir le contenu d'un répertoire

C'est la commmande `ls` suivie du nom du répertoire que tu veux voir. Pareil, tu peux spécifier un chemin absolu ou relatif.

Exemple: 

```
$ ls /usr
bin    include  lib32  local  share  x86_64-linux-gnu
games  lib      lib64  sbin   src
```

La commande `ls` possède plein d'options. Nous n'allons pas toutes les voir, mais il est utile que tu connaisses `ls -lh`:

Exemple: 

```
$ ls -lh
total 24K
drwxr-xr-x 3 axelle axelle 4,0K juil.  7 22:53 boris
-rwxr--r-- 1 axelle axelle  116 juil.  7 22:44 down.sh
drwxr-xr-x 3 axelle axelle 4,0K juil.  6 19:32 john1
drwxr-xr-x 3 axelle axelle 4,0K juil.  7 22:26 john2
drwxr-xr-x 3 axelle axelle 4,0K juil.  8 18:26 unix1
-rwxr--r-- 1 axelle axelle  176 juil.  7 22:44 up.sh
```

Qu'est-ce que tu vois ?

- les lignes qui commencent par un petit `d` te signalent que c'est un répertoire.
- les autres cas sont des fichiers.

Donc, dans l'exemple ci dessus, boris, john1, john2 et unix1 sont des répertoires, tandis que down.sh et up.sh sont des fichiers.


### Voir le contenu d'un fichier

Tu peux biensur l'ouvrir avec `emacs`, mais il y a plus rapide. Tu peux utiliser la commande `cat`.
Pareil, la commande peut etre suivre du chemin absolu vers un fichier ou relatif.

Exemple: 

```
$ cat ./host.conf 
# The "order" line is only used by old versions of the C library.
order hosts,bind
multi on
```


Si le fichier est long, il vaut mieux utiliser la commande `more` qui t'affichera le fichier page par page.

Si tu regardes le contenu d'un programme (on dit aussi un executable), ca va te donner plein de caractères bizarre, car les ordinateurs ne parlent pas la meme langue que nous !

Si tu regardes le contenu d'un répertoire, `cat` va te prévenir que ce n'est pas possible:

```
$ cat /etc
cat: /etc: Is a directory
```






